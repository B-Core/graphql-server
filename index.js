import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import { graphqlExpress, graphiqlExpress } from 'graphql-server-express';
import Schema from './data/schema';

const GraphQLSchema = Schema;
const PORT = 3020;

// options object
const GraphQLOptions = {
  schema: GraphQLSchema,
  // values to be used as context and rootValue in resolvers
  // context?: any,
  // rootValue?: any,
  // function used to format errors before returning them to clients
  // formatError?: Function,
  // additional validation rules to be applied to client-specified queries
  // validationRules?: Array<ValidationRule>,
  // function applied for each query in a batch to format parameters before passing them to `runQuery`
  // formatParams?: Function,
  // function applied to each response before returning data to clients
  // formatResponse?: Function,
  // a boolean option that will trigger additional debug logging if execution errors occur
  // debug?: boolean
}

var app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// bodyParser is needed just for POST.
app.use('/graphql', bodyParser.json(), graphqlExpress({ schema: GraphQLSchema }));
app.use('/graphiql', graphiqlExpress({
  endpointURL: '/graphql',
}));

app.listen(PORT);
