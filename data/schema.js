import { makeExecutableSchema } from 'graphql-tools';

import resolvers from './resolvers';

const schema = `
  type Line {
   _id: Int!
   label: String!
   description: String
   order_id: Int
   lineitem_id: Int
   campaign_id: Int
   service_id: Int
   allowed_rtb: Boolean
   rtb_strategy_id: Int
   currency_cpm_default_bid_max: Float
   timezone: String
   date_start_text: String
   date_end_text: String
   currency_total_budget: Float
   currency_daily_budget: Float
   creative_ids: [Int]
   country_ids: [Int]
   created_at: String
   updated_at: String
  }

  # The schema accept this type of queries
  type Query {
    lines: [Line]
    line(id: ID!): Line
  }

  input LineInput {
    _id: ID!
    label: String!
  }

  type Mutation {
    changeLineLabel (input: LineInput!): Line
  }
`;

export default makeExecutableSchema({
  typeDefs: schema,
  resolvers,
});
