import { find, filter } from 'lodash';
import { pubsub } from './subscriptions';

const lines = [
  { _id: 1, label: 'Line 1' },
  { _id: 2, label: 'Line 2' },
];

const resolveFunctions = {
  Query: {
    lines(_) {
      return lines;
    },
    line(_, { id }) {
      return find(lines, { _id: parseInt(id, 10) });
    }
  },
  Mutation: {
    changeLineLabel(_, data) {
      const line = find(lines, { _id: parseInt(data.input._id, 10) });
      console.log(`changing label of ${line.label}`);
      if (!line) {
        throw new Error(`Couldn't find line with id ${data.input._id}`);
      }
      line.label = data.input.label;
      pubsub.publish('changeLineLabel', line);
      return line;
    },
  },
};

export default resolveFunctions;
